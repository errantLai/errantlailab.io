---
layout: page
title: About Me
description: Summary of the website and its author
---
## whoami
I'm Chris, a computer [engineering student](https://www.ece.queensu.ca/) from Canada. I'm a fan of [organization tools](http://toodledo.com), [Zebra Sarasa pens](https://www.jetpens.com/blog/zebra-sarasa-a-comprehensive-guide/pt/652), and reading. On campus I have had the pleasure of working with the [Fuel Cell Team](http://qfct.ca/) for three years, and currently am with the [BioMedical Innovation Team](https://www.qbitqueensu.com/). 

## hostname
This website is hosted via gitlab, which supports the static site generator Jekyll. 

I am using the minimal and responsive theme monochrome, by github user [dyutibarma](https://github.com/dyutibarma). I appreciate the hard focus it puts on the content. 
For more details, you can check out its [repository on Github](https://github.com/dyutibarma/monochrome). 

## life \-\-help
Wear sunscreen

<!-- ![monochrome](img/monochrome01.png "hover_text") -->